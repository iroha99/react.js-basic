import React, { useEffect, useState } from 'react'

const Localstorage = (key, initialValue) => {
    const [data, setData] = useState(initialValue)

    useEffect(() => {
        const localStorageData = localStorage.getItem(key)
        if(!!localStorageData)
        {
            const parsedData = JSON.parse(localStorageData)
            setData(parsedData)
        }
    },[])

    const handleChangeData = value => {
        const stringify = JSON.stringify(value)
        localStorage.setItem(key, stringify)
    }

    const handleDeleteData = () => {
        localStorage.removeItem(key)
        setData(null)
    }

    return [data, handleChangeData, handleDeleteData]

}

export default Localstorage