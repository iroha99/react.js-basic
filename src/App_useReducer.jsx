import React, { useReducer } from 'react'


// state itu awal init, action itu payload dari dispatch
const reducer = (state, action) => {
    console.log(state,action)
    return {
        ...action.payload
    }
}

const initialState = { 
    nama: "Johan" 
}


//Best practice

const CONSTANT_TYPE = {
    EDIT_UMUR : "EDIT_UMUR",
    EDIT_HOBBY: "EDIT_HOBBY",
    EDIT_NAMA: "EDIT_NAMA",
    EDIT_KOTA: "EDIT_KOTA",
}

const reducerBestPractice = (init, action) => {
    switch(action.type) {
        case CONSTANT_TYPE.EDIT_UMUR: {
            return {
                ...init,
                umur : action.payload
            }
        }
        case CONSTANT_TYPE.EDIT_NAMA: {
            return {
                ...init,
                nama : action.payload.nama
            }
        }
        case CONSTANT_TYPE.EDIT_HOBBY: {
            return {
                ...init,
                hobby : action.payload.hobby
            }
        }
        case CONSTANT_TYPE.EDIT_KOTA: {
            return {
                ...init,
                kota : action.payload.kota
            }
        }

        default:
            return init
    }
}

const initialStateBestPractice = {
    nama : "Johan",
    hobby : "ngoding",
    umur : 30,
    kota : "Jakarta"
}
//

const App_useReducer = () => {
    
    // const [state,dispatch] = useReducer(reducer, initialState)
    const [state,dispatch] = useReducer(
        reducerBestPractice, 
        initialStateBestPractice
    )

  return (
    <div>
        <div>App_useReducer</div>
        <pre>
            {JSON.stringify(state,null,2)}
        </pre>
        <button onClick={() => dispatch(
            {
                type: "TAMBAH",
                payload: {
                    nama: "Irving"
                }
            }
        )}>Ubah Data</button>
        <div>
            <button onClick={() => dispatch(
                {
                    type: CONSTANT_TYPE.EDIT_UMUR,
                    payload: state.umur + 1
                }
            )}>Ubah Umur</button>
            
            <button onClick={() => dispatch(
                {
                    type: CONSTANT_TYPE.EDIT_KOTA,
                    payload: {kota : "Bandung"}
                }
            )}>Ubah Kota Bandung</button>

            <button onClick={() => dispatch(
                {
                    type: CONSTANT_TYPE.EDIT_KOTA,
                    payload: {kota : "Jakarta"}
                }
            )}>Ubah Kota Jakarta</button>

            <button onClick={() => dispatch(
                {
                    type: CONSTANT_TYPE.EDIT_HOBBY,
                    payload: {hobby : "tidur"}
                }
            )}>Ubah hobby</button>

            <button onClick={() => dispatch(
                {
                    type: CONSTANT_TYPE.EDIT_NAMA,
                    payload: {nama : "Irving"}
                }
            )}>Ubah Nama</button>

        </div>
    </div>
  )
}

export default App_useReducer