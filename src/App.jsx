import React, { useState } from 'react'
import Appusestate from './App_usestate'
import Appuseeffect from './App_useEffect'
import App_useReducer from './App_useReducer'
import App_useContext from './App_useContext'
import App_customHook from './App_customHook'

const App = () => {

  // return <Appusestate />
  // return <Appuseeffect />
  // return <App_useReducer />
  // return <App_useContext />
  return <App_customHook />

}

export default App