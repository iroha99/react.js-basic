import React, { createContext, useContext, useState } from 'react'

const UserContext = createContext(null)

const App_useContext = () => {
    const userInfoState = { name: "Johan" }
    const [userInfo, setUserInfo] = useState(userInfoState)

    return (
        <UserContext.Provider value={userInfo}>
            <NavbarContext />
            <div>App Context</div>
            <InfoUserComp />
            <button onClick={ () => setUserInfo( prev => ({ ...prev,name: "Irving" })) }>Ganti Nama</button>
        </UserContext.Provider>
    )
}

const ButtonProfileContext = () => {
    const user = useContext(UserContext)
    return (
        <button>Hallo {user.name}</button>
    )
}

const NavbarContext = () => {
    return (
        <nav>
            <ul>
                <li>Home</li>
                <li>About</li>
                <ButtonProfileContext/>
            </ul>
        </nav>
    )
}

const InfoUserComp = () => {
    const user = useContext(UserContext)
    return (
        <div>
            {JSON.stringify(user,null,2)}
        </div>
    )
}

export default App_useContext