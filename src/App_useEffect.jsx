import React, { useEffect, useState } from 'react'

const App = () => {
    const [nilai,setNilai] = useState(0)

    useEffect(() => {
      if(nilai % 2 === 0 && nilai != 0)
        alert('nilai genap')
    },[nilai])

  return (
    <div>
        <div>Nilai : {nilai}</div>
        <button onClick={() => setNilai(prev => prev + 1)}>Tambah</button>
        {nilai % 2 == 0 && <Unmountcomponent />}
    </div>
  )

}

export default App



const Unmountcomponent = () => {

  useEffect(() => {
    alert("Genap")
    return () => {
      alert("Bukan genap, render ulang")
    }
  },[])

  return <div>Nilai genap</div>

}