import React, { useState } from 'react'

const App = () => {
  const nama = 'Jo'
  const [umur,setUmur] = useState(30)
  const [info,setInfo] = useState({
    pekerjaan: 'Developer',
    hobby: "ngoding"
  })

  const handleInfo = jenisHobby => {
    setInfo(prevInfo => {
      return {
        ...prevInfo,
        hobby: jenisHobby
      }
    })
  }

  
  const onHanldeClick = () => {
    setUmur(umur + 1)
  }

  return (
    <div>
      <h1>Data Pegawai</h1>

      <table>
        <thead>
          <tr>
            <td>ID</td>
            <td>Nama</td>
            <td>Umur</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>{nama}</td>
            <td>{umur}</td>
          </tr>
        </tbody>
      </table>
      <button onClick={onHanldeClick}>Tambah umur</button>
      <button onClick={() => handleInfo("main")}>Ganti hobby main</button>
      <button onClick={() => handleInfo("makan")}>Ganti hobby makan</button>
      <pre>
        {JSON.stringify(info,null,2)}
      </pre>
    </div>
  )
}

export default App