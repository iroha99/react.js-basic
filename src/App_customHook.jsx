import React, { useState } from 'react'
import Localstorage from './hooks/Localstorage'

const App_customHook = () => {
    const [jwt, setJwt, deleteJwt] = Localstorage("ACCESS_TOKEN_JWT","")
    const [inputValue, setInputValue] = useState("")

    const handleJwt = () => {
        setJwt(inputValue)
    }

    return (
        <div>
            <div>
                <input 
                value={inputValue}
                onChange={event => { setInputValue(event.target.value) }}
                type="text" />
            </div>
            <button onClick={handleJwt}>Ganti Value</button>
            <button onClick={deleteJwt}>Hapus Value</button>
            <div>
                JWT: {jwt}
            </div>
        </div>
    )
}

export default App_customHook